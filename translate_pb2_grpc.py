# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import translate_pb2 as translate__pb2


class TranslatorStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.translate = channel.stream_stream(
            "/Translator.Translator/translate",
            request_serializer=translate__pb2.TranslateRequest.SerializeToString,
            response_deserializer=translate__pb2.TranslateResponse.FromString,
        )


class TranslatorServicer(object):
    """Missing associated documentation comment in .proto file."""

    def translate(self, request_iterator, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")


def add_TranslatorServicer_to_server(servicer, server):
    rpc_method_handlers = {
        "translate": grpc.stream_stream_rpc_method_handler(
            servicer.translate,
            request_deserializer=translate__pb2.TranslateRequest.FromString,
            response_serializer=translate__pb2.TranslateResponse.SerializeToString,
        ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
        "Translator.Translator", rpc_method_handlers
    )
    server.add_generic_rpc_handlers((generic_handler,))


# This class is part of an EXPERIMENTAL API.
class Translator(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def translate(
        request_iterator,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.stream_stream(
            request_iterator,
            target,
            "/Translator.Translator/translate",
            translate__pb2.TranslateRequest.SerializeToString,
            translate__pb2.TranslateResponse.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )
