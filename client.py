import queue
from argparse import ArgumentParser

import grpc

import translate_pb2_grpc
import translate_pb2

LANGUAGES = {
    "cz": translate_pb2.Language.CZ,
    "sk": translate_pb2.Language.SK,
    "en": translate_pb2.Language.EN,
}

channel = grpc.insecure_channel("localhost:50051")
stub = translate_pb2_grpc.TranslatorStub(channel)


def send_iter(filename, src, dest):
    with open(filename) as f:
        text = f.read().strip().split()

    for word in text:
        yield translate_pb2.TranslateRequest(
            to_translate=word,
            src=LANGUAGES[src],
            dest=LANGUAGES[dest],
        )


parser = ArgumentParser()
parser.add_argument("filename", help="file with text for translation")
parser.add_argument(
    "-s", "--src-lang", help="source language", choices="cz sk en".split(), default="cz"
)
parser.add_argument(
    "-d",
    "--dest-lang",
    help="destination language",
    choices="cz sk en".split(),
    default="en",
)
args = parser.parse_args()
responses = stub.translate(
    send_iter(args.filename, src=args.src_lang, dest=args.dest_lang)
)

print(" ".join(map(lambda r: r.translated, responses)))
