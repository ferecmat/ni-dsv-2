# gRPC translator

## Installation

```sh
pip install -r requirements.txt
```

## Running

### Server

```sh
python server.py
```

### Client

```sh
$ python client.py -h
usage: client.py [-h] [-s {cz,sk,en}] [-d {cz,sk,en}] filename

positional arguments:
  filename              file with text for translation

optional arguments:
  -h, --help            show this help message and exit
  -s {cz,sk,en}, --src-lang {cz,sk,en}
                        source language
  -d {cz,sk,en}, --dest-lang {cz,sk,en}
                        destination language
```

```sh
python client.py text.txt
```
