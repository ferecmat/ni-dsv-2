from concurrent import futures

import grpc
from googletrans import Translator

import translate_pb2
import translate_pb2_grpc


LANGUAGES = {
    translate_pb2.Language.CZ: "cs",
    translate_pb2.Language.SK: "sk",
    translate_pb2.Language.EN: "en",
}


class TranslatorServicer(translate_pb2_grpc.TranslatorServicer):
    def __init__(self, translator):
        self.translator = translator

    def translate(self, request_iterator, context):
        for req in request_iterator:
            print(".", end="")
            yield translate_pb2.TranslateResponse(
                translated=self.translator.translate(
                    req.to_translate, src=LANGUAGES[req.src], dest=LANGUAGES[req.dest]
                ).text
            )


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    translate_pb2_grpc.add_TranslatorServicer_to_server(
        TranslatorServicer(Translator()), server
    )
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
